import random

DIRECTIONS = (
    (0,1), (1,1), (1,0), (1,-1), (0,-1), (-1,-1), (-1,0), (-1,1)
)

boggle_dice = (
    'RIFOBX',
    'IFEHEY',
    'DENOWS',
    'UTOKND',
    'HMSRAO',
    'LUPETS',
    'ACITOA',
    'YLGKUE',
    'QBMJOA',
    'EHISPN',
    'VETIGN',
    'BALIYT',
    'EZAVND',
    'RALESC',
    'UWILRG',
    'PACEMD'
)

def all_words():
    with open('words.txt', 'r') as stream:
        return [line.strip() for line in stream.readlines() if len(line.strip()) > 2]

def roll():
    '''
    returns a list of letters like a roll of the boggle dice
    '''
    letters = [ random.choice(die) for die in boggle_dice ]
    for i in range(0, len(letters)):
        if letters[i] == 'Q':
            letters[i] = 'QU'
    random.shuffle(letters)
    return letters

class Trie(object):

    def __init__(self, letter=''):
        self.is_word = False 
        self.children = {}
        self.letter = letter

    def add(self, word):
        if not word:
            # we ARE the last letter
            self.is_word = True
        else:
            if word.startswith('QU'):
                first = 'QU'
                remainder = word[2:]
            else:
                first = word[:1]
                remainder = word[1:]
            if not first in self.children:
                self.children[first] = Trie(first)
            self.children[first].add(remainder)
    
    def all_words(self):
        words = []
        if self.is_word:
            words.append(self.letter)
        for next_letter in self.children:
            words = words + [ self.letter + word for word in self.children[next_letter].all_words() ]
        return words

    def is_final(self):
        return not self.children

def load_trie(words):
    
    root = Trie()
    for word in words:
        root.add(word.upper())
    return root

class Board(object):
    '''
    the board, with its tiles
    '''

    def __init__(self):
        self.height = 4 
        self.width = 4 
        self.tiles = [] 
        letters = roll()
        for x in range(0,self.width):
            col = []
            self.tiles.append(col)
            for y in range(0, self.height):
                col.append(Tile(x,y,letters.pop()))
    
    def all_tiles(self):
        all = []
        for col in self.tiles:
            all = all + col 
        return all 

    def tile_at(self, x, y):
        if 0 <= x < board.width and 0 <= y < board.height:
            return self.tiles[x][y]
        else:
            return None

    def __str__(self):
        rows = []
        for y in range(0, self.height):
            row = "  ".join([ f'{tile.letter:2}' for tile in [ self.tile_at(x, y) for x in range(0,self.width) ] ])
            rows.append(row)
        return '\n'.join(rows)

class Tile(object):

    def __init__(self, x, y, letter):
        self.x = x
        self.y = y
        self.letter = letter
    
    def __str__(self):
        return f'[{self.letter} at {self.x},{self.y}]'


def solve_board(board, trie):
    words = set()
    for tile in board.all_tiles():
        for word in solve_tile(board, trie, tile):
            words.add(word)
    return words

def solve_tile(board, trie, tile, path=[]):
    path = path + [(tile.x, tile.y)]
    # print(f'Trying {tile.x},{tile.y}({tile.letter}) with path {path}')
    words = set()
    if trie.is_word:
        words.add(tile.letter)
        # print(tile.letter, path)
    if not trie.is_final():
        for direction in DIRECTIONS:
            next_tile_x = tile.x + direction[0]
            next_tile_y = tile.y + direction[1]
            next_tile = board.tile_at(next_tile_x, next_tile_y) 
            if next_tile and not (next_tile.x, next_tile.y) in path:
                # if abs(tile.x-next_tile.x) > 1 or abs(tile.y-next_tile.y) > 1:
                #     print(f'tile {tile.x},{tile.y} => tile {next_tile.x},{next_tile.y} by way of {direction}')
                #     print(path)
                next_letter = next_tile.letter 
                if next_letter in trie.children:
                    next_trie = trie.children[next_letter]
                    for next_word in solve_tile(board, next_trie, next_tile, path):
                        words.add(trie.letter + next_word)
        
    return words

if __name__ == '__main__':
    board = Board()
    print(board)
    tr = load_trie(all_words())
    words = list(solve_board(board, tr))
    words.sort()
    print(words)

    score = sum([len(word)-2 for word in words])
    print(f'Score: {score}')